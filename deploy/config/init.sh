#!/bin/sh

vault server -address=https://omdidb.cxi.tul.cz:8200 -config=/vault/config/vault.hcl

vault operator init -tls-skip-verify -key-shares=1 -key-threshold=1 > /tmp/.vault-init
# Extract unseal key and root token
grep 'Unseal' /tmp/.vault-init | awk '{ print $NF }' > /vault/config/.vault-unseal-key
grep 'Root Token' /tmp/.vault-init | awk '{ print $NF }' > /vault/config/.vault-root-token
cp /vault/config/.vault-root-token /root/.vault-token

vault operator unseal -tls-skip-verify "$(cat /vault/config/.vault-unseal-key)"
