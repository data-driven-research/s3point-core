# S3Point Core - a core services for S3Point browser

## Description

Web service developed within the CESNET project S3Point implemented at CXI TUL. It complements a DDR ecosystem. The backbone of the web application is the Java framework Spring Boot. As a repository of data needed for the application is used HashiCorp Vault (key-value engine). The application is built using Gradle, containerized using Docker, and deployed using
Docker Compose. 

[Detailed information about tool in separate wiki](../../../wikis/home).

## Deployment Guide

This guide provides detailed instructions for deploying the S3Point Core web service with Docker Compose and configuring HashiCorp Vault for secure data management.

---

### 1. Configuration

1. **Download and Arrange Deployment Files**:  
   Begin by downloading the `deploy` directory to your target server. This directory should contain:
   - **docker-compose.yaml**: Defines Docker Compose configuration.
   - **application.properties**: Configures application-specific settings for S3Point Core.
   - **.env**: Contains environment variables for Docker and the application, including tokens and credentials.
   - **keystore and ssl Directories**: Place public/private keys and certificates here.
     - **keystore**: Stores keys in Java format.
     - **ssl**: Stores certificates and keys in PEM format.

2. **S3 and Vault Access Configuration**:  
   In `docker-compose.yaml`, `application.properties`, and `.env`, locate the placeholder values (marked as `<<>>`) and replace them with:
   - **Vault access**: For securely storing and retrieving sensitive application data.
   - **S3 access**: Required for backend access to Amazon S3.

3. **Set Certificates and Keys**:  
   Ensure the paths to certificates and keys are correctly set in the `keystore` and `ssl` directories.

---

### 2. Initial Deployment and First-time Vault Setup

1. **Start Containers**:  
   Run the following command to start the application containers:
   ```bash
   docker compose up --wait


This will initialize the S3Point Core and Vault containers. The `--wait` flag ensures Docker waits until all containers are fully up and running.

### 2. Vault Configuration (First-time Setup):
1. Open the Vault GUI by navigating to `https://your-domain.com:8201`.

2. Unseal Vault:
Vault is initially sealed for security. To unseal:
- Enter the unseal keys generated during initialization (multiple keys may be required).
- Store these keys securely for future use, as they are needed if Vault is restarted.

3. Obtain a Root Token:
After unsealing, Vault issues a root token for initial access. Store this token securely, as it’s needed for configuring Vault and must be provided to the application.

4. Set Up Key-Value (KV) Database:
- Create a Key-Value (KV) secrets engine called `kv` (see Vault KV documentation).
- Populate the database with required credentials and secrets:
  - **e-infra ID** (record name): The ID associated with your infrastructure.
  - **accessKey** and **secretKey** (key-value pairs): Corresponding S3 credentials.

## 3. Restart Application with Vault Token

1. Insert Vault Token in .env:
Copy the root token obtained in step 2 into the `.env` file under `VAULT_TOKEN`.

2. Restart the Web Service Container:
- Terminate the existing S3Point Core container:
  ```bash
  docker kill s3point-core-1

This restart initializes the application with access to Vault, allowing S3Point Core to read secrets from the KV database.

Maintenance Note: Unsealing Vault After Updates
Vault Auto-Unsealing:
The current setup does not support Vault auto-unsealing. After any Vault update or restart, it will return to a sealed state. Repeat the unsealing process in the GUI or command line as outlined in Step 2. Always keep unseal keys accessible to avoid prolonged downtime due to a locked Vault after updates.



## Development

To build and deploy as a Docker image locally, you first need to fill *develop/config/application.properties* and
*develop/.env* files.

Then for local develoment instance run  
``
./local-deploy.sh
``

***

To build a Java jar you need to run the following:  
``
./build-release/build.sh test
``

Then in the *develop* directory, run:  
``
docker compose up --wait
``

For a production build, you need to run the following:  
``
./build-release/build.sh {target version}
``  
Note: The target versions used by our developer team are
**production** and **stage**.

The container registry is used to release
on [Docker Hub](https://hub.docker.com/r/cxiomi/s3point-core) with the hub name **cxiomi**, where the
images are uploaded for subsequent deployment. The application is developed so that the actual deployment can be done
with only a change of configuration files in the *deploy* directory. In the case of modifying the code and building an
image for your institution, you need to use your container repository.

The release can be triggered by:  
``
./build-release/release.sh {hub} {target version}
``

The application can be built and released at the same time using the following:  
``
./build-release/build-and-release.sh {hub} {target version}
``



## Authors

Jan Kočí - lead, architect  
Věnceslav Chumchal - developer, architect  
David Vobruba - developer  
Jakub Zach - consultant  

## License

Apache-2.0
