@echo off
set VERSION=%1
set FILE=S3PointCore-%VERSION%.jar

gradlew build -Pversion="%VERSION%" && copy "build\libs\%FILE%" "S3PointCore.jar"