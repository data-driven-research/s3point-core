#!/bin/bash

VERSION=$1
FILE=S3PointCore-$VERSION.jar

./gradlew build -Pversion="$VERSION"
cp "build/libs/$FILE" "S3PointCore.jar"
