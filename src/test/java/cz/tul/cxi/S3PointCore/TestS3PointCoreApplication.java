package cz.tul.cxi.S3PointCore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestS3PointCoreApplication {

	public static void main(String[] args) {
		SpringApplication.from(S3PointCoreApplication::main).with(TestS3PointCoreApplication.class).run(args);
	}

}
