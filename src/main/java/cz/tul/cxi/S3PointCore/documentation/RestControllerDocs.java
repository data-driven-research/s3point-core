package cz.tul.cxi.S3PointCore.documentation;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class RestControllerDocs {

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation(
            tags = {"credentials"},
            summary = "Create credentials",
            description = "Creates new credentials for a user. Uses the Einfra ID as the key to store the Access Key and Secret Key. The API uses the Einfra ID to authenticate the user.",
            requestBody = @RequestBody(
                    description = "Credentials object",
                    required = true,
                    content = @Content(
                            mediaType = "application/json",
                            examples = {@ExampleObject(
                                    value = """
                                            {
                                                 "einfraId": "einfraId",
                                                 "s3AccessKey": "accessKey",
                                                 "s3SecretKey": "secretKey"
                                             }"""
                            )}
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "timestamp": "2024-07-25T15:28:56.121+00:00",
                                                          "status": 400,
                                                          "error": "Bad Request",
                                                          "path": "/v1/credentials/create"
                                                      }"""
                                    )}
                            )}
                    )
            }
    )
    public @interface CreateCredentialsDocs {}

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation(
            tags = {"credentials"},
            summary = "Update credentials",
            description = "Updates existing credentials for a specified Einfra ID.",
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "Credentials object",
                    required = true,
                    content = @Content(
                            mediaType = "application/json",
                            examples = {@ExampleObject(
                                    value = """
                                            {
                                                 "einfraId": "einfraId",
                                                 "s3AccessKey": "newAccessKey",
                                                 "s3SecretKey": "newSecretKey"
                                             }"""
                            )}
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation"
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "timestamp": "2024-07-25T15:28:56.121+00:00",
                                                          "status": 400,
                                                          "error": "Bad Request",
                                                          "path": "/v1/credentials/update"
                                                      }"""
                                    )}
                            )}
                    )
            }
    )
    public @interface UpdateCredentialsDocs {}

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation(
            tags = {"buckets"},
            summary = "List buckets",
            description = "Lists all available buckets for a given Einfra ID user.",
            parameters = {
                    @Parameter(
                            name = "einfraId",
                            description = "Used to authenticate the user",
                            required = true,
                            in = ParameterIn.HEADER
                    ),
            },
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                         "totalRecords": 2,
                                                         "curPage": 1,
                                                         "data": [
                                                             {
                                                                 "name": "67418106-2-test"
                                                             },
                                                             {
                                                                 "name": "slozka"
                                                             }
                                                         ]
                                                     }"""
                                    )}
                            )}
                    ),
                    @ApiResponse(
                            responseCode = "500",
                            description = "Internal server error",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                         "timestamp": "2024-07-25T15:19:38.415+00:00",
                                                         "status": 500,
                                                         "error": "Internal Server Error",
                                                         "path": "/v1/buckets/list"
                                                     }"""
                                    )}
                            )}
                    )
            }
    )
    public @interface ListBucketsDocs {}

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation(
            tags = {"buckets"},
            summary = "Create bucket",
            description = "Creates a new bucket. Einfra ID and Tenant ID are used to create the bucket policy.",
            parameters = {
                    @Parameter(
                            name = "tenantId",
                            description = "Tenant ID",
                            required = true,
                            in = ParameterIn.HEADER
                    ),
                    @Parameter(
                            name = "einfraId",
                            description = "Einfra ID",
                            required = true,
                            in = ParameterIn.HEADER
                    )
            },
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "S3Bucket object. Contains the bucket name.",
                    required = true,
                    content = @Content(
                            mediaType = "application/json",
                            examples = {@ExampleObject(
                                    value = """
                                            {
                                                 "name": "newbucket"
                                             }"""
                            )}
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Successful operation",
                            content = {@Content(
                                    mediaType = "text/plain",
                                    examples = {@ExampleObject(
                                            value = "Successfully created bucket newbucket"
                                    )}
                            )}
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "timestamp": "2024-07-25T15:28:56.121+00:00",
                                                          "status": 400,
                                                          "error": "Bad Request",
                                                          "path": "/v1/buckets/create"
                                                      }"""
                                    )}
                            )}
                    )
            }
    )
    public @interface CreateBucketDocs {}

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation(
            tags = {"datasets"},
            summary = "List datasets",
            description = "Lists all datasets in a given bucket (first-level folders in the bucket).",
            parameters = {
                    @Parameter(
                            name = "einfraId",
                            description = "Used to authenticate the user",
                            required = true,
                            in = ParameterIn.HEADER
                    ),
            },
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "S3Bucket object. Contains the bucket name.",
                    required = true,
                    content = @Content(
                            mediaType = "application/json",
                            examples = {@ExampleObject(
                                    value = """
                                            {
                                                 "name": "slozka"
                                             }"""
                            )}
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "curPage": 1,
                                                          "totalRecords": 3,
                                                          "data": [
                                                              {
                                                                  "bucketName": "slozka",
                                                                  "name": "folder1/",
                                                                  "prefix": ""
                                                              },
                                                              {
                                                                  "bucketName": "slozka",
                                                                  "name": "folder2/",
                                                                  "prefix": ""
                                                              },
                                                              {
                                                                  "bucketName": "slozka",
                                                                  "name": "folder3/",
                                                                  "prefix": ""
                                                              }
                                                          ]
                                                      }"""
                                    )}
                            )}
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "timestamp": "2024-07-25T15:28:56.121+00:00",
                                                          "status": 400,
                                                          "error": "Bad Request",
                                                          "path": "/v1/datasets/list"
                                                      }"""
                                    )}
                            )}
                    )
            }
    )
    public @interface ListDatasetsDocs {}

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation(
            tags = {"datasets"},
            summary = "WIP Create dataset",
            description = "Creates a dataset in a specified bucket. Currently not working. Einfra ID and Tenant ID are used to create the dataset policy.",
            parameters = {
                    @Parameter(
                            name = "tentantId",
                            description = "Tenant ID",
                            required = true,
                            in = ParameterIn.HEADER
                    ),
                    @Parameter(
                            name = "einfraId",
                            description = "Einfra ID",
                            required = true,
                            in = ParameterIn.PATH
                    )
            },
            requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "S3Object dataset. Contains the bucket name and dataset name. The prefix is an empty string - datasets are first-level folders in the bucket thus they have no prefix. Dataset names must end with a forward slash.",
                    required = true,
                    content = @Content(
                            mediaType = "application/json",
                            examples = {@ExampleObject(
                                    value = """
                                            {
                                                  "bucketName": "slozka",
                                                  "name": "dataset/",
                                                  "prefix": ""
                                              }"""
                            )}
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Successful operation",
                            content = {@Content(
                                    mediaType = "text/plain",
                                    examples = {@ExampleObject(
                                            value = "Successfully created dataset dataset/ in bucket slozka"
                                    )}
                            )}
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "timestamp": "2024-07-25T15:28:56.121+00:00",
                                                          "status": 400,
                                                          "error": "Bad Request",
                                                          "path": "/v1/datasets/create"
                                                      }"""
                                    )}
                            )}
                    )
            }
    )
    public @interface CreateDatasetDocs {}

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation(
            tags = {"datasets"},
            summary = "WIP Share dataset",
            description = "Shares a dataset. Currently not working. Einfra ID and Tenant ID are used to share the dataset.",
            parameters = {
                    @Parameter(
                            name = "tentantId",
                            description = "Tenant ID",
                            required = true,
                            in = ParameterIn.HEADER
                    ),
                    @Parameter(
                            name = "einfraId",
                            description = "Einfra ID",
                            required = true,
                            in = ParameterIn.PATH
                    )
            },
            requestBody = @RequestBody(
                    description = "S3Object dataset. Contains the bucket name and dataset name. The prefix is an empty string - datasets are first-level folders in the bucket thus they have no prefix.",
                    required = true,
                    content = @Content(
                            mediaType = "application/json",
                            examples = {@ExampleObject(
                                    value = """
                                            {
                                                  "bucketName": "slozka",
                                                  "name": "datasettest",
                                                  "prefix": ""
                                              }"""
                            )}
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Successful operation",
                            content = {@Content(
                                    mediaType = "text/plain",
                                    examples = {@ExampleObject(
                                            value = "Successfully shared dataset datasettest in bucket slozka"
                                    )}
                            )}
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "timestamp": "2024-07-25T15:28:56.121+00:00",
                                                          "status": 400,
                                                          "error": "Bad Request",
                                                          "path": "/v1/datasets/share"
                                                      }"""
                                    )}
                            )}
                    )
            }
    )
    public @interface ShareDatasetDocs {}

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation(
            tags = {"objects"},
            summary = "List objects",
            description = "Lists all objects in a given bucket and a path (dataset or a folder).",
            parameters = {
                    @Parameter(
                            name = "einfraId",
                            description = "Used to authenticate the user",
                            required = true,
                            in = ParameterIn.HEADER
                    ),
            },
            requestBody = @RequestBody(
                    description = """
                            S3Object object. Contains the bucket name and the path to the object. The full path to an object is the prefix + name. To list all objects in a dataset "mydataset", use either
                            {"bucketName": "mybucket", "name": "mydataset/", "prefix": ""} or {"bucketName": "mybucket", "name": "/", "prefix": "mydataset"}.
                            Same goes for listing objects in folders. Note: the name value must not be an empty string, prefix can. Example:
                            {"bucketName": "mybucket", "name": "myfolder/", "prefix": "mydataset/"} or {"bucketName": "mybucket", "name": "/", "prefix": "mydataset/myfolder"}.""",
                    required = true,
                    content = @Content(
                            mediaType = "application/json",
                            examples = {
                                    @ExampleObject(
                                        value = """
                                                {
                                                      "bucketName": "slozka",
                                                      "name": "mydataset/",
                                                      "prefix": ""
                                                  }"""
                                    ),
                                    @ExampleObject(
                                        value = """
                                                {
                                                      "bucketName": "slozka",
                                                      "name": "/",
                                                      "prefix": "mydataset"
                                                  }"""
                                    ),
                                    @ExampleObject(
                                        value = """
                                                {
                                                      "bucketName": "slozka",
                                                      "name": "myfolder/",
                                                      "prefix": "mydataset/"
                                                  }"""
                                    ),
                                    @ExampleObject(
                                        value = """
                                                {
                                                      "bucketName": "slozka",
                                                      "name": "/",
                                                      "prefix": "mydataset/myfolder"
                                                  }"""
                                    )
                            }
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {
                                            @ExampleObject(
                                                value = """
                                                        {
                                                            "totalRecords": 3,
                                                            "curPage": 1,
                                                            "data": [
                                                                {
                                                                    "bucketName": "slozka",
                                                                    "name": "file.txt",
                                                                    "prefix": "mydataset/folder/"
                                                                },
                                                                {
                                                                    "bucketName": "slozka",
                                                                    "name": "folder1/",
                                                                    "prefix": "mydataset/folder/"
                                                                },
                                                                {
                                                                    "bucketName": "slozka",
                                                                    "name": "folder2/",
                                                                    "prefix": "mydataset/folder/"
                                                                }
                                                            ]
                                                        }
                                                        """
                                            ),
                                            @ExampleObject(
                                                    value = """
                                                        {
                                                            "totalRecords": 2,
                                                            "curPage": 1,
                                                            "data": [
                                                                {
                                                                    "bucketName": "slozka",
                                                                    "name": "folder/",
                                                                    "prefix": "mydataset/"
                                                                },
                                                                {
                                                                    "bucketName": "slozka",
                                                                    "name": "file.png",
                                                                    "prefix": "mydataset/"
                                                                }
                                                            ]
                                                        }
                                                        """
                                            )
                                    }
                            )}
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "timestamp": "2024-07-25T15:28:56.121+00:00",
                                                          "status": 400,
                                                          "error": "Bad Request",
                                                          "path": "/v1/objects/list"
                                                      }"""
                                    )}
                            )}
                    )
            }
    )
    public @interface ListObjectsDocs {}

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation(
            tags = {"objects"},
            summary = "Create object",
            description = "Creates a new file/folder in a specified bucket and path (dataset or another folder).",
            parameters = {
                    @Parameter(
                            name = "einfraId",
                            description = "Used to authenticate the user",
                            required = true,
                            in = ParameterIn.HEADER
                    ),
            },
            requestBody = @RequestBody(
                    description = "S3Object object. Same as in the list objects operation.",
                    required = true,
                    content = @Content(
                            mediaType = "application/json",
                            examples = {
                                    @ExampleObject(
                                            value = """
                                                    {
                                                          "bucketName": "slozka",
                                                          "name": "mynewfolder/",
                                                          "prefix": "dataset/"
                                                    }"""
                                    ),
                                    @ExampleObject(
                                            value = """
                                                    {
                                                          "bucketName": "slozka",
                                                          "name": "mynewfile.txt",
                                                          "prefix": "dataset/"
                                                    }"""
                                    )
                            }
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Successful operation",
                            content = {@Content(
                                    mediaType = "text/plain",
                                    examples = {@ExampleObject(
                                            value = "Successfully created object mynewfolder/ in bucket slozka"
                                    )}
                            )}
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "timestamp": "2024-07-25T15:28:56.121+00:00",
                                                          "status": 400,
                                                          "error": "Bad Request",
                                                          "path": "/v1/objects/create"
                                                      }"""
                                    )}
                            )}
                    )
            }
    )
    public @interface CreateObjectDocs {}

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation()
    public @interface DownloadObjectMetadataDocs {}

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @Operation(
            tags = {"objects"},
            summary = "Download object",
            description = "Creates a temporary public link to download an object. The link is valid for a specified amount of time. Used to download files only. Generates links for invalid files.",
            parameters = {
                    @Parameter(
                            name = "einfraId",
                            description = "Used to authenticate the user",
                            required = true,
                            in = ParameterIn.HEADER
                    ),
                    @Parameter(
                            name = "signatureDurationMinutes",
                            description = "Duration of the link validity in minutes. Default is 60 minutes.",
                            required = false,
                            in = ParameterIn.QUERY
                    )
            },
            requestBody = @RequestBody(
                    description = "S3Object object. Same as in the list objects operation.",
                    required = true,
                    content = @Content(
                            mediaType = "application/json",
                            examples = {
                                    @ExampleObject(
                                            value = """
                                                    {
                                                          "bucketName": "slozka",
                                                          "name": "file.txt",
                                                          "prefix": "mydataset/"
                                                    }"""
                                    )
                            }
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful operation",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "url": "https://example.com"
                                                      }"""
                                    )}
                            )}
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Bad request",
                            content = {@Content(
                                    mediaType = "application/json",
                                    examples = {@ExampleObject(
                                            value = """
                                                    {
                                                          "timestamp": "2024-07-25T15:28:56.121+00:00",
                                                          "status": 400,
                                                          "error": "Bad Request",
                                                          "path": "/v1/objects/download"
                                                      }"""
                                    )}
                            )}
                    )
            }
    )
    public @interface DownloadObjectDocs {}
}
