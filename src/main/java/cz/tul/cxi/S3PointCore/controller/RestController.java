package cz.tul.cxi.S3PointCore.controller;

import cz.tul.cxi.S3PointCore.model.Credentials;
import cz.tul.cxi.S3PointCore.model.S3Bucket;
import cz.tul.cxi.S3PointCore.model.S3Object;
import cz.tul.cxi.S3PointCore.service.CredentialsService;
import cz.tul.cxi.S3PointCore.service.S3Service;
import cz.tul.cxi.S3PointCore.documentation.RestControllerDocs.*;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/v1")
public class RestController {
    public final CredentialsService credentialsService;
    public final S3Service s3Service;

    public RestController(CredentialsService credentialsService,
                          S3Service s3Service) {
        this.credentialsService = credentialsService;
        this.s3Service = s3Service;
    }

    @PutMapping(value = "/credentials/create", consumes = "application/json")
    @CreateCredentialsDocs
    public ResponseEntity<String> createCredentials(@Valid @RequestBody Credentials credentials) {
        credentialsService.putCredentials(credentials);
        return ResponseEntity.status(HttpStatusCode.valueOf(201)).build();
    }

    @PatchMapping(value = "/credentials/update", consumes = "application/json")
    @UpdateCredentialsDocs
    public ResponseEntity<String> updateCredentials(@Valid @RequestBody Credentials credentials) {
        credentialsService.patchCredentials(credentials);
        return ResponseEntity.status(HttpStatusCode.valueOf(200)).build();
    }

    @GetMapping(value = "/buckets/list", produces = "application/json")
    @ListBucketsDocs
    public ResponseEntity<String> listBuckets() {
        /*
         Lists all buckets.
        */
        return new ResponseEntity<>(s3Service.listBuckets(), HttpStatusCode.valueOf(200));
    }

    @PutMapping(value = "/buckets/create", consumes = "application/json")
    @CreateBucketDocs
    public ResponseEntity<String> createBucket(@Valid @RequestBody S3Bucket s3Bucket,
                                               @RequestHeader(value = "tenantId") String tenantId,
                                               @RequestHeader(value = "einfraId") String einfraId){
        s3Service.createBucket(s3Bucket, tenantId, einfraId);
        return new ResponseEntity<>("Successfully created bucket %s".formatted(s3Bucket.getName()), HttpStatusCode.valueOf(201));
    }

    @PostMapping(value = "/datasets/list", produces = "application/json", consumes = "application/json")
    @ListDatasetsDocs
    public ResponseEntity<String> listDatasets(@Valid @RequestBody S3Bucket s3Bucket) {
        /*
         Lists all datasets in a given bucket (first-level folders in the bucket).
        */
        return new ResponseEntity<>(s3Service.listDatasets(s3Bucket), HttpStatusCode.valueOf(200));
    }

    @PutMapping(value = "/datasets/create", consumes = "application/json")
    @CreateDatasetDocs
    public ResponseEntity<String> createDataset(@Valid @RequestBody S3Object s3Dataset,
                                                @RequestHeader(value = "tenantId") String tenantId,
                                                @RequestParam(value = "einfraId") String einfraId) {
        s3Service.createDataset(s3Dataset, tenantId, einfraId);
        return new ResponseEntity<>("Successfully created dataset %s in bucket %s".formatted(s3Dataset.getName(), s3Dataset.getBucketName()), HttpStatusCode.valueOf(201));
    }

    @PutMapping(value = "/datasets/share", consumes = "application/json")
    @ShareDatasetDocs
    public ResponseEntity<String> shareDataset(@Valid @RequestBody S3Object s3Dataset,
                                               @RequestHeader(value = "tenantId") String tenantId,
                                               @RequestParam(value = "einfraId") String einfraId) {
        s3Service.giveUserReadWriteToS3Dataset(s3Dataset, tenantId, einfraId);
        return new ResponseEntity<>("Successfully shared dataset %s in bucket %s".formatted(s3Dataset.getName(), s3Dataset.getBucketName()), HttpStatusCode.valueOf(200));
    }

    @PostMapping(value = "/objects/list", produces = "application/json", consumes = "application/json")
    @ListObjectsDocs
    public ResponseEntity<String> listObjects(@Valid @RequestBody S3Object s3Object) {
        /*
         Lists all objects in a object or dataset (folder).
        */
        String objects = s3Service.listObjects(s3Object);
        if (objects == null) {
            return new ResponseEntity<>("Bucket not found", HttpStatusCode.valueOf(404));
        }
        return new ResponseEntity<>(objects, HttpStatusCode.valueOf(200));
    }

    @PutMapping(value = "/objects/create", consumes = "application/json")
    @CreateObjectDocs
    public ResponseEntity<String> createFolder(@Valid @RequestBody S3Object s3Object) {
        s3Service.createFolder(s3Object);
        return new ResponseEntity<>("Successfully created object %s in bucket %s".formatted(s3Object.getName(), s3Object.getBucketName()), HttpStatusCode.valueOf(201));
    }

    @PostMapping(value = "/objects/metadata", produces = "application/json", consumes = "application/json")
    //@DownloadObjectMetadataDocs
    public ResponseEntity<String> downloadObjectMetadata(@Valid @RequestBody S3Object s3Object) {
        return new ResponseEntity<>(s3Service.getObjectMetadata(s3Object), HttpStatusCode.valueOf(200));
    }

    @PostMapping(value = "/objects/download", consumes = "application/json")
    @DownloadObjectDocs
    public ResponseEntity<String> shareObjectWithTemporaryPublicLink(@Valid @RequestBody S3Object s3Object,
                                                                     @RequestParam(name = "signatureDurationMinutes", required = false, defaultValue = "60") int signatureDurationMinutes) {
        return new ResponseEntity<>(s3Service.getObjectDownloadPublicLink(s3Object, signatureDurationMinutes), HttpStatusCode.valueOf(200));
    }

    @PostMapping(value = "/objects/upload", consumes = "application/json")
    public ResponseEntity<String> uploadObjectWithTemporaryPublicLink(@Valid @RequestBody S3Object s3Object) {
        return new ResponseEntity<>(s3Service.getObjectUploadPublicLink(s3Object), HttpStatusCode.valueOf(200));
    }

    @PostMapping(value = "/objects/downloadUrls", produces = "application/json", consumes = "application/json")
    public String getDownloadUrls(@Valid @RequestBody S3Object s3Object) {
        return s3Service.getDownloadUrls(s3Object);
    }
}
