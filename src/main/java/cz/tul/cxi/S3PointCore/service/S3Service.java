package cz.tul.cxi.S3PointCore.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.tul.cxi.S3PointCore.component.JSONComponent;
import cz.tul.cxi.S3PointCore.component.S3Component;
import cz.tul.cxi.S3PointCore.model.S3Bucket;
import cz.tul.cxi.S3PointCore.model.S3Object;
import cz.tul.cxi.S3PointCore.model.BucketPolicy;
import cz.tul.cxi.S3PointCore.model.Statement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class S3Service {

    private final JSONComponent jsonComponent;

    private final S3Component s3Component;

    private final ObjectMapper mapper;

    public S3Service(JSONComponent jsonComponent,
                     S3Component s3Component) {
        this.jsonComponent = jsonComponent;
        this.s3Component = s3Component;
        this.mapper = new ObjectMapper();
    }

    public String listBuckets() {
        try {
            return jsonComponent.createSimpleFrontendJSON(s3Component.listBuckets());
        } catch (Exception e) {
            log.error("Error creating frontend JSON", e);
            throw new IllegalStateException("Error creating frontend JSON", e);
        }
    }

    public void createBucket(S3Bucket s3Bucket, String tenantId, String einfraId) {
        s3Component.createBucket(s3Bucket.getName());
        s3Component.putDefaultBucketCors(s3Bucket.getName());
        s3Component.updateBucketPolicy(s3Bucket.getName(), constructDefaultPolicy(s3Bucket.getName(), tenantId, einfraId));
    }

    public String listDatasets(S3Bucket s3Bucket) {
        List<S3Object> datasets = s3Component.listDatasets(s3Bucket.getName());
        return jsonComponent.createSimpleFrontendJSON(datasets);
    }

    public void createDataset(S3Object s3Object,
                              String tenantId,
                              String einfraId) {
        s3Component.createEmpty(s3Object.getBucketName(), s3Object.getPrefix(), s3Object.getName());
        giveUserReadWriteToS3Dataset(s3Object, tenantId, einfraId);
    }

    public String listObjects(S3Object s3Object) {
        List<S3Object> objects = s3Component.listCurrentLevelObjects(s3Object.getBucketName(), s3Object.getPrefix(), s3Object.getName());
        return jsonComponent.createSimpleFrontendJSON(objects);

    }

    public void createFolder(S3Object s3Object) {
        // if a path ends with a "/" it's a folder, otherwise it's a file
        s3Component.createEmpty(s3Object.getBucketName(), s3Object.getPrefix(), s3Object.getName());
    }

    public String getObjectDownloadPublicLink(S3Object s3Object, int signatureDurationMinutes) {
        String url = s3Component.createDownloadUrl(s3Object.getBucketName(), s3Object.getPrefix(), s3Object.getName(), signatureDurationMinutes);
        try {
            return mapper.writeValueAsString(Map.of("url", url));
        } catch (JsonProcessingException e) {
            log.error("Error creating download URL", e);
            throw new RuntimeException("Error creating download URL", e);
        }
    }

    public String getDownloadUrls(S3Object s3Object) {
        Set<S3Object> files = s3Component.listAllObjects(s3Object.getBucketName(), s3Object.getPrefix(), s3Object.getName());
        files.removeIf(it -> it.getName().endsWith("/"));
        List<Map<String, String>> fileUrls = files.stream().map(it -> Map.of("url", s3Component.createDownloadUrl(
                it.getBucketName(), it.getPrefix(), it.getName(), 60), "prefix", it.getPrefix())).toList();
        try {
            return mapper.writeValueAsString(Map.of("fileUrls", fileUrls));
        } catch (JsonProcessingException e) {
            log.error("Error creating file URLs", e);
            throw new RuntimeException("Error creating file URLs", e);
        }
    }

    public String getObjectUploadPublicLink(S3Object s3Object) {
        return s3Component.createUploadUrl(s3Object.getBucketName(), s3Object.getPrefix(), s3Object.getName(), Map.of());
    }

    public String getObjectMetadata(S3Object s3Object) {
        try {
            return mapper.writeValueAsString(s3Component.getObjectMetadata(s3Object.getBucketName(), s3Object.getPrefix(), s3Object.getName()));
        } catch (JsonProcessingException e) {
            log.error("Error converting object metadata", e);
            throw new RuntimeException("Error converting object metadata", e);
        }
    }

    private String constructDefaultPolicy(String bucketName, String tenantId, String einfraId) {
        BucketPolicy bucketPolicyTemplate = jsonComponent
                .readJsonFileToBucketPolicy("policies/default_bucket_policy.json");
        BucketPolicy bucketPolicy = new BucketPolicy(new ArrayList<>());
        String awsPrincipal = String.format("arn:aws:iam::%s:user/%s", tenantId, einfraId);

        for (Statement statementTemplate : bucketPolicyTemplate.Statement()) {
            Statement statement = updateBucketStatements(statementTemplate, bucketName, awsPrincipal);
            bucketPolicy.Statement().add(statement);
        }

        String policyString = jsonComponent.convertBucketPolicyToJsonString(bucketPolicy);
        log.debug("Policy string: " + policyString);

        return policyString;

    }

    private Statement updateBucketStatements(Statement statementTemplate,
                                             String bucketName,
                                             String awsPrincipal) {
        return new Statement(
                statementTemplate.Sid(),
                statementTemplate.Action(),
                statementTemplate.Effect(),
                Map.of("AWS", List.of(awsPrincipal)),
                statementTemplate.Resource().stream().map(it ->
                        it.replace("BUCKET_NAME", bucketName)).toList(),
                statementTemplate.Condition()
        );
    }

    public void giveUserReadWriteToS3Dataset(S3Object s3dataset,
                                             String tenantId,
                                             String einfraId) {
        BucketPolicy bucketPolicy = jsonComponent.convertJsonStringToBucketPolicy(
                s3Component.getCurrentBucketPolicy(s3dataset.getBucketName())
        );
        String awsPrincipal = String.format("arn:aws:iam::%s:user/%s", tenantId, einfraId);
        log.debug(awsPrincipal);

        boolean updated = false;
        for (Statement statement : bucketPolicy.Statement()) {
            if (statement.Sid().contains("ListBucket") && !statement.Principal().get("AWS").contains(awsPrincipal)) {
                statement.Principal().get("AWS").add(awsPrincipal);
            }
            if (statement.Sid().contains(s3dataset.getName()) && !statement.Principal().get("AWS").contains(awsPrincipal)) {
                statement.Principal().get("AWS").add(awsPrincipal);
                updated = true;
            }
        }

        if (!updated) {
            List<Statement> statementTemplates = jsonComponent
                    .readJsonFileToBucketPolicy("policies/share_statements.json").Statement();
            for (Statement statementTemplate : statementTemplates) {
                Statement statement = updateDatasetStatements(statementTemplate, s3dataset.getName(), s3dataset.getBucketName(), awsPrincipal);
                bucketPolicy.Statement().add(statement);
            }
        }

        String policyString = jsonComponent.convertBucketPolicyToJsonString(bucketPolicy);
        log.debug(policyString);
        s3Component.updateBucketPolicy(s3dataset.getBucketName(), policyString);
    }

    private Statement updateDatasetStatements(Statement statementTemplate,
                                              String datasetFolder,
                                              String bucketName,
                                              String awsPrincipal) {
        statementTemplate.Condition().forEach((it, it2) -> it2.forEach((it3, it4) ->
                it4.replaceAll(it5 -> it5.replace("DATASET_FOLDER", datasetFolder))));

        return new Statement(
                statementTemplate.Sid().replace("DATASET_FOLDER", datasetFolder),
                statementTemplate.Action(),
                statementTemplate.Effect(),
                Map.of("AWS", List.of(awsPrincipal)),
                statementTemplate.Resource().stream().map(it -> it
                                .replace("BUCKET_NAME", bucketName)
                                .replace("DATASET_FOLDER", datasetFolder))
                        .toList(),
                statementTemplate.Condition()
        );
    }
}

