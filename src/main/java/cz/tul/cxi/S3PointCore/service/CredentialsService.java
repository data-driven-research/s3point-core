package cz.tul.cxi.S3PointCore.service;

import cz.tul.cxi.S3PointCore.model.Credentials;
import org.springframework.stereotype.Service;
import org.springframework.vault.core.VaultKeyValueOperations;
import org.springframework.vault.core.VaultKeyValueOperationsSupport;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

@Service
public class CredentialsService {
    private final VaultTemplate vaultTemplate;

    public CredentialsService(VaultTemplate vaultTemplate) {
        this.vaultTemplate = vaultTemplate;
    }

    public void putCredentials(Credentials credentials) {
        VaultKeyValueOperations keyValueOperations = vaultTemplate.opsForKeyValue("kv",
                VaultKeyValueOperationsSupport.KeyValueBackend.KV_1);
        keyValueOperations.put(
                credentials.getEinfraId(),
                Map.of("accessKey", credentials.getS3AccessKey(), "secretKey", credentials.getS3SecretKey()));
    }

    public void patchCredentials(Credentials credentials) {
        // KV 1 does not support patching, so we overwrite the existing entry
        putCredentials(credentials);
    }

    public Credentials readCredentials(String eInfraId) {
        VaultKeyValueOperations keyValueOperations = vaultTemplate.opsForKeyValue("kv",
                VaultKeyValueOperationsSupport.KeyValueBackend.KV_1);
        VaultResponse response = keyValueOperations.get(eInfraId);
        assert response != null;
        return new Credentials(
                eInfraId,
                (String) Objects.requireNonNull(response.getData()).get("accessKey"),
                (String) response.getData().get("secretKey"));
    }
}
