package cz.tul.cxi.S3PointCore.model;

import java.util.List;

public record BucketPolicy(String Id,
                           String Version,
                           List<cz.tul.cxi.S3PointCore.model.Statement> Statement) {

    public BucketPolicy(List<Statement> Statement) {
        this("PolicyId", "2012-10-17", Statement);
    }
}
