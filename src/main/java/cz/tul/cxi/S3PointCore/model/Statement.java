package cz.tul.cxi.S3PointCore.model;

import java.util.List;
import java.util.Map;

public record Statement(String Sid,
                        List<String> Action,
                        String Effect,
                        Map<String, List<String>> Principal,
                        List<String> Resource,
                        Map<String, Map<String, List<String>>> Condition) {
}
