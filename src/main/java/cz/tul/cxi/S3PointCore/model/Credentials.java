package cz.tul.cxi.S3PointCore.model;


import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Credentials {
    @NotBlank(message = "einfraId must not be blank.")
    private String einfraId;

    @NotBlank(message = "s3AccessKey must not be blank.")
    private String s3AccessKey;

    @NotBlank(message = "s3SecretKey must not be blank.")
    private String s3SecretKey;
}
