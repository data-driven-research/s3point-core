package cz.tul.cxi.S3PointCore.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class S3Object {
    @NotBlank(message = "bucketName must not be blank.")
    private String bucketName;

    @NotBlank(message = "(file) name must not be blank.")
    private String name; // current patch of path aka display name

    @NotNull(message = "(file) prefix must not be null.")
    private String prefix; // almost full path relative to bucketName

    @Override
    public int hashCode() {
        return bucketName.hashCode() + name.hashCode() + prefix.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (object == this) {
            return true;
        }
        if (object.getClass() != this.getClass()) {
            return false;
        }
        S3Object s3Object = (S3Object) object;
        return s3Object.getBucketName().equals(this.getBucketName()) &&
                s3Object.getName().equals(this.getName()) &&
                s3Object.getPrefix().equals(this.getPrefix());
    }
}
