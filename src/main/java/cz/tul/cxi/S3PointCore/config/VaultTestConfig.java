package cz.tul.cxi.S3PointCore.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.core.VaultTemplate;

// https://docs.spring.io/spring-vault/reference/vault/vault-repositories.html

@Configuration
@Profile(value = {"test"})
class VaultTestConfig {

    @Value("${vault.port}")
    private int vaultPort;

    @Value("${vault.token}")
    private String vaultToken;

    @Bean
    public VaultTemplate vaultTemplate() {
        VaultEndpoint endpoint = new VaultEndpoint();
        endpoint.setScheme("http");
        endpoint.setHost("vault");
        endpoint.setPort(vaultPort);
        return new VaultTemplate(endpoint, new TokenAuthentication(vaultToken));
    }
}

