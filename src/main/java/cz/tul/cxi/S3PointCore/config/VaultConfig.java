package cz.tul.cxi.S3PointCore.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.vault.authentication.ClientAuthentication;
import org.springframework.vault.authentication.TokenAuthentication;
import org.springframework.vault.client.VaultEndpoint;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.repository.configuration.EnableVaultRepositories;

// https://docs.spring.io/spring-vault/reference/vault/vault-repositories.html

@Configuration
@Profile(value = {"stage", "production"})
class VaultConfig {

    @Value("${vault.port}")
    private int vaultPort;

    @Value("${vault.token}")
    private String vaultToken;

    @Value("${vault.hostname}")
    private String vaultHostname;

    @Bean
    public VaultTemplate vaultTemplate() {
        VaultEndpoint endpoint = new VaultEndpoint();
        endpoint.setScheme("https");
        endpoint.setHost(vaultHostname);
        endpoint.setPort(vaultPort);
        return new VaultTemplate(endpoint, new TokenAuthentication(vaultToken));
    }
}

