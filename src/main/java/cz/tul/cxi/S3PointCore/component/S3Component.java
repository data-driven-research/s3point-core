package cz.tul.cxi.S3PointCore.component;

import cz.tul.cxi.S3PointCore.model.S3Bucket;
import cz.tul.cxi.S3PointCore.model.S3Object;
import org.springframework.stereotype.Component;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedGetObjectRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedPutObjectRequest;
import software.amazon.awssdk.services.s3.presigner.model.PutObjectPresignRequest;

import java.time.Duration;
import java.util.*;

@Component
public class S3Component {
    private final S3Client s3Client;

    public S3Component(S3Client s3Client) {
        this.s3Client = s3Client;
    }

    public void createEmpty(String bucketName,
                            String prefix,
                            String name) {
        PutObjectRequest objectRequest = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(prefix + name)
                .build();
        
        s3Client.putObject(objectRequest, RequestBody.empty());
    }

    public String getCurrentBucketPolicy(String bucketName) {
        GetBucketPolicyRequest policyRequest = GetBucketPolicyRequest.builder()
                .bucket(bucketName)
                .build();

        return s3Client.getBucketPolicy(policyRequest).policy();
    }

    public void updateBucketPolicy(String bucketName,
                                   String newPolicy) {
        PutBucketPolicyRequest policyRequest = PutBucketPolicyRequest.builder()
                .bucket(bucketName)
                .policy(newPolicy)
                .build();

        s3Client.putBucketPolicy(policyRequest);
    }

    public void createBucket(String bucketName) {
        CreateBucketRequest bucketRequest = CreateBucketRequest.builder()
                .bucket(bucketName)
                .createBucketConfiguration(CreateBucketConfiguration.builder()
                        .locationConstraint("storage")
                        .build())
                .build();

        s3Client.createBucket(bucketRequest);
    }

    public void putDefaultBucketCors(String bucketName) {
        PutBucketCorsRequest corsRequest = PutBucketCorsRequest.builder()
                .bucket(bucketName)
                .corsConfiguration(CORSConfiguration.builder()
                        .corsRules(CORSRule.builder()
                                .allowedOrigins("*")
                                .allowedMethods("GET", "PUT", "POST", "DELETE", "HEAD")
                                .maxAgeSeconds(3000)
                                .exposeHeaders("ETag")
                                .allowedHeaders("*")
                                .build())
                        .build())
                .build();

        s3Client.putBucketCors(corsRequest);
    }

    public String createDownloadUrl(String bucketName,
                                    String prefix,
                                    String name,
                                    int signatureDurationMinutes) {
        try (S3Presigner presigner = S3Presigner
                .builder()
                .region(s3Client.serviceClientConfiguration().region())
                .credentialsProvider(s3Client.serviceClientConfiguration().credentialsProvider())
                .endpointOverride(s3Client.serviceClientConfiguration().endpointOverride().orElseThrow())
                .build()
        ) {
            GetObjectRequest objectRequest = GetObjectRequest
                    .builder()
                    .bucket(bucketName)
                    .key(prefix + name)
                    .build();

            GetObjectPresignRequest presignRequest = GetObjectPresignRequest
                    .builder()
                    .signatureDuration(Duration.ofMinutes(signatureDurationMinutes))
                    .getObjectRequest(objectRequest)
                    .build();

            PresignedGetObjectRequest presignedRequest = presigner.presignGetObject(presignRequest);

            return  presignedRequest.url().toExternalForm();
        }
    }

    /* Create a presigned URL to use in a subsequent PUT request */
    public String createUploadUrl(String bucketName,
                                  String prefix,
                                  String name,
                                  Map<String, String> metadata) {
        try (S3Presigner presigner = S3Presigner.builder()
                .region(s3Client.serviceClientConfiguration().region())
                .credentialsProvider(s3Client.serviceClientConfiguration().credentialsProvider())
                .endpointOverride(s3Client.serviceClientConfiguration().endpointOverride().orElseThrow())
                .build()
        ) {
            PutObjectRequest objectRequest = PutObjectRequest.builder()
                    .bucket(bucketName)
                    .key(prefix + name)
                    .metadata(metadata)
                    .build();

            PutObjectPresignRequest presignRequest = PutObjectPresignRequest.builder()
                    .signatureDuration(Duration.ofMinutes(10))// The URL expires in 10 minutes.
                    .putObjectRequest(objectRequest)
                    .build();

            PresignedPutObjectRequest presignedRequest = presigner.presignPutObject(presignRequest);

            return presignedRequest.url().toExternalForm();
        }
    }

    public Map<String, String> getObjectMetadata(String bucketName,
                                                 String prefix,
                                                 String name) {
        HeadObjectRequest headObjectRequest = HeadObjectRequest
                .builder()
                .bucket(bucketName)
                .key(prefix + name)
                .build();

        return s3Client.headObject(headObjectRequest).metadata();
    }


    public List<S3Object> listDatasets(String bucketName) {
        Set<S3Object> files = new HashSet<>();
        String fullPrefix = "";
        ListObjectsV2Request listObjects = ListObjectsV2Request
                .builder()
                .bucket(bucketName)
                .build();
        try {
            ListObjectsV2Response res = s3Client.listObjectsV2(listObjects);
            for (software.amazon.awssdk.services.s3.model.S3Object object : res.contents()) {
                files.add(new S3Object(bucketName, createName(object.key(), fullPrefix), fullPrefix));
            }
            for (CommonPrefix object : res.commonPrefixes()) {
                files.add(new S3Object(bucketName, createName(object.prefix(), fullPrefix), fullPrefix));
            }
            return files.stream().toList();

        } catch (NoSuchBucketException e) {
            throw new IllegalArgumentException("Bucket not found");
        }
    }

    public List<S3Object> listCurrentLevelObjects(String bucketName, String prefix, String name) {
        Set<S3Object> files = new HashSet<>();
        String fullPrefix = prefix + name;
        ListObjectsV2Request listObjects = ListObjectsV2Request
                .builder()
                .bucket(bucketName)
                .prefix(fullPrefix)
                .delimiter("/")
                .build();
        try {
            ListObjectsV2Response res = s3Client.listObjectsV2(listObjects);
            for (software.amazon.awssdk.services.s3.model.S3Object object : res.contents()) {
                files.add(new S3Object(bucketName, createName(object.key(), fullPrefix), fullPrefix));
            }
            for (CommonPrefix object : res.commonPrefixes()) {
                files.add(new S3Object(bucketName, createName(object.prefix(), fullPrefix), fullPrefix));
            }
            return files.stream().toList();

        } catch (NoSuchBucketException e) {
            throw new IllegalArgumentException("Bucket not found");
        }
    }

    public Set<S3Object> listAllObjects(String bucketName, String prefix, String name) {
        Set<S3Object> files = new HashSet<>();
        String fullPrefix = prefix + name;
        ListObjectsV2Request listObjects = ListObjectsV2Request
                .builder()
                .bucket(bucketName)
                .prefix(fullPrefix)
                .build();
        try {
            ListObjectsV2Response res = s3Client.listObjectsV2(listObjects);
            for (software.amazon.awssdk.services.s3.model.S3Object object : res.contents()) {
                String objectName = createName(object.key(), fullPrefix);
                String objectPrefix = object.key().replace(objectName, "");
                // TODO: fix to be able to download all files with directory structure
                files.add(new S3Object(bucketName, objectName, objectPrefix));
            }
            return files;

        } catch (NoSuchBucketException e) {
            throw new IllegalArgumentException("Bucket not found");
        }
    }

    private String createName(String key, String fullPrefix) {
        key = key.replaceFirst(fullPrefix, "");
        String[] splitName = key.split("/");
        String name = splitName[0];
        name = key.endsWith("/") || splitName.length > 1 ? name + "/" : name;
        return name;
    }

    public List<S3Bucket> listBuckets() {
        return s3Client.listBuckets().buckets().stream().map(bucket -> new S3Bucket(bucket.name())).toList();
    }
}
