package cz.tul.cxi.S3PointCore.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.tul.cxi.S3PointCore.model.BucketPolicy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class JSONComponent {
    private final ResourceLoader resourceLoader;
    private final ObjectMapper objectMapper;

    public JSONComponent(ResourceLoader resourceLoader,
                         ObjectMapper objectMapper) {
        this.resourceLoader = resourceLoader;
        this.objectMapper = objectMapper;
    }

    public BucketPolicy readJsonFileToBucketPolicy(String filePath) {
        // Load the JSON file from the classpath
        Resource resource = resourceLoader.getResource("classpath:" + filePath);

        // Use ObjectMapper to convert JSON to Map
        try {
            return objectMapper.readValue(resource.getInputStream(), BucketPolicy.class);
        } catch (IOException e) {
            log.error("Error reading JSON file", e);
            throw new RuntimeException("Error reading JSON file", e);
        }
    }

    public String convertBucketPolicyToJsonString(BucketPolicy mapToConvert)  {
        try {
            return objectMapper.writeValueAsString(mapToConvert);
        } catch (JsonProcessingException e) {
            log.error("Error converting map to JSON string", e);
            throw new RuntimeException("Error converting map to JSON string", e);
        }
    }

    public BucketPolicy convertJsonStringToBucketPolicy(String bucketPolicy) {
        try {
            return objectMapper.readValue(bucketPolicy, BucketPolicy.class);
        } catch (JsonProcessingException e) {
            log.error("Error converting JSON string to map", e);
            throw new RuntimeException("Error converting JSON string to map", e);
        }
    }

    public <T> String createSimpleFrontendJSON(List<T> data)  {
        try {
            return objectMapper.writeValueAsString(
                    Map.of("curPage", 1, "totalRecords", data.size(), "data", data));
        } catch (JsonProcessingException e) {
            log.error("Error creating frontend JSON", e);
            throw new RuntimeException("Error creating frontend JSON", e);
        }
    }
}
