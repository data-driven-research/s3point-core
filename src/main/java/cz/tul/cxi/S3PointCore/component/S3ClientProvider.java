package cz.tul.cxi.S3PointCore.component;

import java.net.URI;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.vault.core.VaultKeyValueOperations;
import org.springframework.vault.core.VaultKeyValueOperationsSupport;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;
import org.springframework.web.context.annotation.RequestScope;

import jakarta.servlet.http.HttpServletRequest;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3Configuration;

@Component
public class S3ClientProvider {

    private final VaultTemplate vaultTemplate;

    @Value("${s3.endpoint_url}")
    private String endpointURL;

    public S3ClientProvider(VaultTemplate vaultTemplate) {
        this.vaultTemplate = vaultTemplate;
    }

    @Bean
    @RequestScope
    public S3Client cesnetS3Client(HttpServletRequest request) {
        String einfraId = request.getHeader("einfraId");

        return S3Client.builder()
                .credentialsProvider(credentialsProvider(einfraId))
                .endpointOverride(URI.create(endpointURL))
                .region(Region.AWS_GLOBAL)
                .serviceConfiguration(S3Configuration.builder()
                        .pathStyleAccessEnabled(true)
                        .build())
                .build();
    }

    private AwsCredentialsProvider credentialsProvider(String einfraId) {
        VaultKeyValueOperations keyValueOperations = vaultTemplate.opsForKeyValue("kv",
                VaultKeyValueOperationsSupport.KeyValueBackend.KV_1);
        VaultResponse response = keyValueOperations.get(einfraId);
        assert response != null;
        String accessKey = (String) Objects.requireNonNull(response.getData()).get("access_key");
        String secretKey = (String) response.getData().get("secret_key");
        return StaticCredentialsProvider
                .create(AwsBasicCredentials.create(accessKey, secretKey));
    }
}
