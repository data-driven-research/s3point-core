package cz.tul.cxi.S3PointCore;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition
public class S3PointCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(S3PointCoreApplication.class, args);
	}
}

